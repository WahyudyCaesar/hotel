# WEB APP SKELETON

## Pendahuluan
Source code ini adalah adalah template untuk membuat sebuah frontend website dengan menggunakan SCSS dan SUSY

## Mulai
Untuk menggunakan project ini berikut caranya:
* git clone https://bungendang@bitbucket.org/doesprogrammer/html-sass-susy.git .
* npm install
* bower install

## Serving
gulp serve